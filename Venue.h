//
//  Venue.h
//  FSMap
//
//  Created by Filip Jakubowski on 06.11.2013.
//  Copyright (c) 2013 Hemnes Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Venue : NSObject <MKAnnotation>

@property CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *cathegory;

-(void)setCoordinateX:(float)x andY:(float)y;

@end
