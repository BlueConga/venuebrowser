//
//  FSQueryManager.m
//  FSMap
//
//  Created by Filip Jakubowski on 13.11.2013.
//  Copyright (c) 2013 Hemnes Consulting. All rights reserved.
//

#import "QueryManager.h"
#import "Venue.h"

#define kForsquareQuery @"https://api.foursquare.com/v2/venues/search?client_id=XUWGNT2ET1UCPOMCD4QOPIMYKQEQIFRLBPXRQXFRW0JSKAWV&client_secret=DEGU5ELI3CV5Z4MIC05I13FL31HWA1CTYQUWFRIMNB1KWO5G"
#define kLLParam @"&ll="
#define kQueryParam @"&query="



@implementation QueryManager
static QueryManager *sharedFSManaged;

+(QueryManager*)sharedManager{
    static BOOL initialized = NO;
    if(!initialized){
        initialized = YES;
        sharedFSManaged = [[QueryManager alloc] init];
    }
    
    return sharedFSManaged;
}


+(NSArray*)getFSSynchronusQueryResultForQuery:(NSString*)query andLocation:(CLLocation*)location{
    
    NSMutableArray *results = [NSMutableArray new];
    NSString *reqString = [NSString stringWithFormat:@"%@%@%.7f,%.7f%@%@", kForsquareQuery,kLLParam, location.coordinate.latitude, location.coordinate.longitude,kQueryParam, query];
    
    NSURL *requestURL = [NSURL URLWithString:reqString];
    NSURLRequest *request = [NSURLRequest requestWithURL:requestURL];
    
    NSError  *error;
    
    NSData *testJSON = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:testJSON options:kNilOptions error:&error];
    NSArray * response = [[(NSDictionary*)[jsonDict objectForKey:@"response"] objectForKey:@"groups"][0] objectForKey:@"items"];
    
    for (NSDictionary* venue in response) {
        NSString *venueName = [venue objectForKey:@"name"];
        //NSLog([NSString stringWithFormat:@"nazwa lokalu: %@", venueName ]);
        
        NSDictionary *location = [venue objectForKey:@"location"];
        
        float lat = [[location objectForKey:@"lat"] floatValue];
        float lng = [[location objectForKey:@"lng"] floatValue];
        
        
        Venue *ven = [Venue new];
        ven.name = venueName;
        //category
        NSArray *categories = [venue objectForKey:@"categories"];
        if([categories count] > 0){
            NSString *category = [((NSDictionary*)[categories objectAtIndex:0]) objectForKey:@"name"];
            ven.cathegory = category;
            
        }
        
        [ven setCoordinateX:lat andY:lng];
        
        [results addObject:ven];
    }
    
    return results;
}

@end
