//
//  FSQueryManager.h
//  FSMap
//
//  Created by Filip Jakubowski on 13.11.2013.
//  Copyright (c) 2013 Hemnes Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface QueryManager : NSObject

+(QueryManager*)sharedManager;

+(NSArray*)getFSSynchronusQueryResultForQuery:(NSString*)query andLocation:(CLLocation*)point;
@end
