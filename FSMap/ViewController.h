//
//  ViewController.h
//  FSMap
//
//  Created by Filip Jakubowski on 06.11.2013.
//  Copyright (c) 2013 Hemnes Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface ViewController : UIViewController <CLLocationManagerDelegate, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet MKMapView* map;

@end
