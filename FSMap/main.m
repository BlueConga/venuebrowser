//
//  main.m
//  FSMap
//
//  Created by Filip Jakubowski on 06.11.2013.
//  Copyright (c) 2013 Hemnes Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
