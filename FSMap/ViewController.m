//
//  ViewController.m
//  FSMap
//
//  Created by Filip Jakubowski on 06.11.2013.
//  Copyright (c) 2013 Hemnes Consulting. All rights reserved.
//

#import "ViewController.h"
#import "QueryManager.h"
#define kRegionKm 10000;

@interface ViewController ()
@property (strong, nonatomic) CLLocationManager *locationManager;
@property BOOL regionUpdated;
@property (strong, nonatomic) CLLocation* location;
@property (strong, nonatomic) NSArray *tmpVenues;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [_locationManager startUpdatingLocation];
    
    _regionUpdated = NO;

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Location delegate methods
// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    // If it's a relatively recent event, turn off updates to save power.
    _location = [locations lastObject];
    if(!_regionUpdated){
        _regionUpdated = YES;
        float range = kRegionKm;
         MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(_location.coordinate, range, range);
         [_map setRegion:viewRegion];
    }
}


#pragma mark UISearchBarDelegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSString *query = searchBar.text;
    if(_regionUpdated){
        [_map removeAnnotations:_tmpVenues];
        _tmpVenues = [QueryManager getFSSynchronusQueryResultForQuery:query andLocation:_location];
        
        [searchBar resignFirstResponder];
        [_map addAnnotations:_tmpVenues];
    }
   
    
}

@end
