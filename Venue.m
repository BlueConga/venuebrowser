//
//  Venue.m
//  FSMap
//
//  Created by Filip Jakubowski on 06.11.2013.
//  Copyright (c) 2013 Hemnes Consulting. All rights reserved.
//

#import "Venue.h"

@implementation Venue

@synthesize coordinate, name, cathegory;

-(id)init{
    self = [super init];
    if(self) {
        
    }
    
    return self;
}


-(void)setCoordinateX:(float)x andY:(float)y{
    coordinate = CLLocationCoordinate2DMake(x, y);
}

-(NSString*)title{
    return self.name;
}

-(NSString*)subtitle{
    return self.cathegory;
}
@end
